#pragma once
#include "Token.h"

class CLexer
{
public:
	CLexer(const std::string& text);
	bool HasNext() const;
	Token GetNext();
private:
	void SkipWhitespaces();
	void SkipSingleLineComment();
	void SkipMultiLineComment();

	Token ParseSeparator(char firstChar);
	Token ParseIdentifier(char firstChar);
	Token ParseNumber(char firstChar);
	Token ParseString();
	Token ParseChar();

	bool IsSeparator(char ch) const;
	bool IsKeyword(const std::string& identifier) const;
	char GetNextChar();
	char PeekNext() const;
	size_t GetInputSignal(char ch) const;

	const std::string m_text;
	size_t m_position;
};
