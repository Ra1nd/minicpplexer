#pragma once
#include "stdafx.h"

enum TokenKind
{
	T_IDENTIFIER,
	T_KEYWORD,
	T_INTEGER,
	T_REAL,
	T_FLOAT,

	T_LEFT_PARENTHESIS, // '('
	T_RIGHT_PARENTHESIS, // ')'
	T_LEFT_CURLY_BRACE, // '{'
	T_RIGHT_CURLY_BRACE, // '}'
	T_LEFT_SQUARE_BRACKET, // '['
	T_RIGHT_SQUARE_BRACKET, // ']'

	T_ARITHMETIC_OPERATOR, // '+', '-', '/', '*', '%'
	T_ASSIGMENT_ARITHMETIC_OPERATOR, // '+=', '-=', '/=', '*=', '%='
	T_ASSIGMENT, // '='

	T_EQUALITY, // '=='
	T_INEQUALITY, // '!='
	T_COMPARISON, // '>', '<', '<=', '>='

	T_EXCLAMATION_MARK, // '!'

	T_COMMA, // ','
	T_SEMICOLON, // ';'

	T_CHAR,
	T_STRING,
	
	T_EOF,
	T_ERROR
};

struct Token
{
	Token(const TokenKind& kind = T_ERROR, const std::string& value = "")
		: kind(kind)
		, value(value)
	{}

	TokenKind kind;
	std::string value;
};
