#include "stdafx.h"

#include "FileReader.h"
#include "Token.h"
#include "Lexer.h"
#include "TokenWriter.h"

int main(int argc, char *argv[])
{
	std::vector<Token> tokens;
	try
	{
		std::string inputFileName;
		std::string outputFileName;
		if (2 == argc || 3 == argc)
		{
			inputFileName = argv[1];
		}
		if (3 == argc)
		{
			outputFileName = argv[2];
		}

		CFileReader reader(inputFileName);
		std::string text = reader.ReadAllFile();
		CLexer lexer = CLexer(text);
		
		while (lexer.HasNext())
		{
			tokens.push_back(lexer.GetNext());
		}

		CTokenWriter writer(outputFileName);
		writer.WriteToFile(tokens);
	}
	catch (const std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
	}

	return EXIT_SUCCESS;
}
