#include "stdafx.h"
#include "FileReader.h"

namespace
{
	const std::string DEFAULT_INPUT_FILE_NAME = "input.txt";
}

CFileReader::CFileReader(const std::string& fileName)
{
	if (!fileName.empty())
	{
		m_inputStream.open(fileName);
		if (!m_inputStream.is_open())
		{
			std::cout << "Error opening " << fileName << std::endl
				<< "Try to open default file(" << DEFAULT_INPUT_FILE_NAME << ")" << std::endl;
			m_inputStream.open(DEFAULT_INPUT_FILE_NAME);
		}
	}
	else
	{
		m_inputStream.open(DEFAULT_INPUT_FILE_NAME);
	}
}

void CFileReader::Open(const std::string& fileName)
{
	m_inputStream.open(fileName);
}

bool CFileReader::IsOpen() const
{
	return m_inputStream.is_open();
}

std::string CFileReader::ReadAllFile()
{
	if (!IsOpen())
	{
		throw std::invalid_argument("File does not open");
	}

	std::string result;
	std::string line;

	while (std::getline(m_inputStream, line))
	{
		result += line + "\n";
	}

	return result;
}
