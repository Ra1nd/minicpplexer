#pragma once
#include "Token.h"

class CTokenWriter
{
public:
	CTokenWriter(const std::string& fileName = "");
	void Open(const std::string& fileName);
	bool IsOpen() const;
	void WriteToFile(std::vector<Token>& tokens);
private:
	void WriteToken(const Token& token);
	std::string GetTokenKindStr(const Token& token);

	std::ofstream m_outputStream;
};
