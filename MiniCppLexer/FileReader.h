#pragma once

class CFileReader
{
public:
	CFileReader(const std::string& fileName = "");
	void Open(const std::string& fileName);
	bool IsOpen() const;
	std::string ReadAllFile();
private:
	std::ifstream m_inputStream;
};
