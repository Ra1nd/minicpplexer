#include "stdafx.h"
#include "TokenWriter.h"

namespace
{
	const std::string DEFAULT_OUTPUT_FILE_NAME = "output.txt";

	const std::map<TokenKind, std::string> TOKEN_NAMES
	{
		{ T_IDENTIFIER, "Identifier" },
		{ T_KEYWORD, "Keyword" },
		{ T_INTEGER, "Integer" },
		{ T_REAL, "Real" },
		{ T_FLOAT, "Float" },
		{ T_LEFT_PARENTHESIS, "Left parenthesis" },
		{ T_RIGHT_PARENTHESIS, "Right parenthesis" },
		{ T_LEFT_CURLY_BRACE, "Left curly brace" },
		{ T_RIGHT_CURLY_BRACE, "Right curly brace" },
		{ T_LEFT_SQUARE_BRACKET, "Left square bracket" },
		{ T_RIGHT_SQUARE_BRACKET, "Right square bracket" },
		{ T_ARITHMETIC_OPERATOR, "Operator" },
		{ T_ASSIGMENT_ARITHMETIC_OPERATOR, "Operator with assigment" },
		{ T_ASSIGMENT, "Assigment" },
		{ T_EQUALITY, "Equality" },
		{ T_INEQUALITY, "Inequality" },
		{ T_COMPARISON, "Comparison" },
		{ T_EXCLAMATION_MARK, "Exclamation mark" },
		{ T_COMMA, "Comma" },
		{ T_SEMICOLON, "Semicolon" },
		{ T_CHAR, "Char" },
		{ T_STRING, "String" },
		{ T_EOF, "Eof" },
		{ T_ERROR, "Error" }
	};
}

CTokenWriter::CTokenWriter(const std::string& fileName)
{
	if (!fileName.empty())
	{
		m_outputStream.open(fileName);
		if (!m_outputStream.is_open())
		{
			std::cout << "Error opening " << fileName << " for writing" << std::endl
				<< "Try to open default(" << DEFAULT_OUTPUT_FILE_NAME << ")" << std::endl;
			m_outputStream.open(DEFAULT_OUTPUT_FILE_NAME);
		}
	}
	else
	{
		m_outputStream.open(DEFAULT_OUTPUT_FILE_NAME);
	}
}

void CTokenWriter::Open(const std::string& fileName)
{
	m_outputStream.open(fileName);
}

bool CTokenWriter::IsOpen() const
{
	return m_outputStream.is_open();
}

void CTokenWriter::WriteToken(const Token& token)
{
	m_outputStream << "Token kind: \"" << GetTokenKindStr(token) << "\"" << std::endl
		<< "value: \"" << token.value << "\"" << std::endl
		<< "----------------------------------------------" << std::endl;
}

void CTokenWriter::WriteToFile(std::vector<Token>& tokens)
{
	for (const Token& token : tokens)
	{
		WriteToken(token);
	}
}

std::string CTokenWriter::GetTokenKindStr(const Token& token)
{
	const auto tmp = TOKEN_NAMES.find(token.kind);

	return tmp != TOKEN_NAMES.end() ? tmp->second : "Unknown";
}
