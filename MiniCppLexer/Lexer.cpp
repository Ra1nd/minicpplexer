#include "stdafx.h"
#include "Lexer.h"

namespace
{
	enum numberRecognizerState
	{
		S_INT,
		S_FLOAT,
		S_REAL_ERROR,
		S_REAL_MARK,
		S_ERROR,
		S_OK,
		S_REAL,
		S_DOT
	};

	enum inputSignals
	{
		INPUT_DIGIT,
		INPUT_DOT,
		INPUT_E,
		INPUT_MARK,
		INPUT_SEPARATOR,
		INPUT_OTHER
	};

	const std::set<std::string> KEYWORDS =
	{
		"void", "int", "float", "char", "string", "bool",
		"if", "else",
		"for", "while", "do", "break", "continue",
		"return", "NULL", "const",
		"true", "false"
	};
	const std::map<char, TokenKind> SEPARATORS =
	{
		{ ';', T_SEMICOLON },
		{ ',', T_COMMA },
		{ '(', T_LEFT_CURLY_BRACE },
		{ ')', T_RIGHT_CURLY_BRACE },
		{ '{', T_LEFT_PARENTHESIS },
		{ '}', T_RIGHT_PARENTHESIS },
		{ '[', T_LEFT_SQUARE_BRACKET },
		{ ']', T_RIGHT_SQUARE_BRACKET },
		{ '+', T_ARITHMETIC_OPERATOR },
		{ '-', T_ARITHMETIC_OPERATOR },
		{ '*', T_ARITHMETIC_OPERATOR },
		{ '/', T_ARITHMETIC_OPERATOR },
		{ '%', T_ARITHMETIC_OPERATOR },
		{ '=', T_ASSIGMENT },
		{ '>', T_COMPARISON },
		{ '<', T_COMPARISON },
		{ '!', T_EXCLAMATION_MARK }
	};

	const std::vector<std::vector<std::pair <size_t, TokenKind>>> NUMBER_RECOGNIZER =
	{
		{ {S_INT, T_INTEGER }, { S_FLOAT, T_FLOAT }, { S_REAL, T_REAL }, { S_REAL, T_REAL }, { S_ERROR, T_ERROR }, { }, { S_REAL, T_REAL } ,{ S_FLOAT, T_FLOAT}},
		{ {S_FLOAT, T_FLOAT }, { S_ERROR, T_FLOAT }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { }, { S_ERROR, T_ERROR },{ S_ERROR, T_ERROR } },
		{ { S_REAL_ERROR, T_REAL }, { S_REAL_ERROR, T_REAL }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { }, { S_ERROR, T_ERROR },{ S_ERROR, T_ERROR } },
		{ { S_OK, T_INTEGER }, { S_OK, T_FLOAT }, { S_REAL_MARK, T_REAL }, { S_ERROR, T_ERROR }, { S_OK, T_ERROR }, { }, { S_OK, T_REAL } ,{ S_OK, T_ERROR } },
		{ { S_OK, T_INTEGER }, { S_OK, T_FLOAT }, { S_OK, T_ERROR }, { S_OK, T_ERROR }, { S_OK, T_ERROR }, { }, { S_OK, T_REAL },{ S_OK, T_ERROR }},
		{ { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { S_ERROR, T_ERROR }, { }, { S_ERROR, T_ERROR },{S_ERROR, T_ERROR } }
	};

	const char SINGLE_QUOTE = '\'';
	const char DOUBLE_QUOTE = '"';
	const char EOLN_SYMBOL = '\n';
	const char SLASH = '/';
	const char BACKSLASH = '\\';
	const char ASTERISK = '*';
	const char UNDERSCORE = '_';
	const char LESS_SIGN = '<';
	const char GREATER_SIGN = '>';
	const char DOT = '.';
	
	const size_t MAX_INDENTIFIER_LENGTH = 255;
}

CLexer::CLexer(const std::string& text)
	: m_text(text)
	, m_position(0)
{
}

bool CLexer::HasNext() const
{
	return m_position < m_text.size();
}

Token CLexer::GetNext()
{
	Token token;

	SkipWhitespaces();

	bool isSkipComment = true;
	while (isSkipComment)
	{
		isSkipComment = false;
		if (HasNext())
		{
			char next = PeekNext();
			if (next == SLASH)
			{
				++m_position;
				char futureSymbol = PeekNext();
				if (futureSymbol == SLASH || futureSymbol == ASTERISK)
				{
					isSkipComment = true;
					if (PeekNext() == SLASH)
					{
						SkipSingleLineComment();
					}
					else
					{
						SkipMultiLineComment();
					}
				}
				else
				{
					token.kind = T_ARITHMETIC_OPERATOR;
					token.value = next;
				}
			}
		}
		SkipWhitespaces();
	}

	if (HasNext() && T_ERROR == token.kind)
	{
		const char firstChar = GetNextChar();

		if (IsSeparator(firstChar))
		{
			token = ParseSeparator(firstChar);
		}
		else if (isalpha(firstChar) || firstChar == UNDERSCORE)
		{
			token = ParseIdentifier(firstChar);
		}
		else if (isdigit(firstChar) || firstChar == DOT)
		{ 
			token = ParseNumber(firstChar);
		}
		else if (firstChar == DOUBLE_QUOTE)
		{
			token = ParseString();
		}
		else if (firstChar == SINGLE_QUOTE)
		{
			token = ParseChar();
		}
		else
		{
			token.value = firstChar;
		}
	}
	else if (T_ARITHMETIC_OPERATOR != token.kind)
	{
		token.kind = T_EOF;
	}

	return token;
}

void CLexer::SkipWhitespaces()
{
	while (HasNext() && isspace(PeekNext()))
	{
		++m_position;
	}
}

void CLexer::SkipSingleLineComment()
{
	while (HasNext() && EOLN_SYMBOL != PeekNext())
	{
		++m_position;
	}
}

void CLexer::SkipMultiLineComment()
{
	while (HasNext())
	{
		++m_position;
		if (ASTERISK == PeekNext())
		{
			++m_position;
			if (SLASH == PeekNext())
			{
				++m_position;
				break;
			}
		}
	}
}

Token CLexer::ParseSeparator(char firstChar)
{
	Token result;

	const auto tmp = SEPARATORS.find(firstChar);
	if (SEPARATORS.end() == tmp)
	{
		throw new std::invalid_argument("Separator not found");
	}

	result.kind = tmp->second;
	result.value = tmp->first;
	
	return result;
}

Token CLexer::ParseIdentifier(char firstChar)
{
	Token result;
	result.kind = T_IDENTIFIER;
	result.value += firstChar;

	while (HasNext())
	{
		char next = PeekNext();
		if (IsSeparator(next) || isspace(next) || next == SINGLE_QUOTE || next == DOUBLE_QUOTE)
		{
			break;
		}
		else if (!isalnum(next) && UNDERSCORE != next)
		{
			result.kind = T_ERROR;
			break;
		}

		result.value += GetNextChar();
	}
	
	if (result.kind == T_ERROR)
	{
		char next = PeekNext();
		while (HasNext() && !IsSeparator(next) && !isspace(next) && next != SINGLE_QUOTE && next != DOUBLE_QUOTE)
		{
			result.value += GetNextChar();
			next = PeekNext();
		}
	}
	else if (IsKeyword(result.value))
	{
		result.kind = T_KEYWORD;
	}
	else if (result.value.length() >= MAX_INDENTIFIER_LENGTH)
	{
		result.kind = T_ERROR;
	}

	return result;
}

Token CLexer::ParseNumber(char currentChar)
{
	Token result;
	bool isDot = (currentChar == DOT);

	result.value = currentChar;
	result.kind = isDot ? T_ERROR :T_INTEGER;

	size_t state = (currentChar == DOT) ? S_DOT : S_INT;
	while (HasNext() && state != S_OK)
	{
		currentChar = PeekNext();
		size_t inputSignal = GetInputSignal(currentChar);
		std::pair<size_t, TokenKind> temp = NUMBER_RECOGNIZER.at(inputSignal).at(state);
		
		state = temp.first;
		result.kind = temp.second;
		if (state != S_OK)
		{
			result.value += GetNextChar();
		}
	}

	return result;
}

Token CLexer::ParseString()
{
	Token result;
	result.kind = T_STRING;

	while (HasNext())
	{
		char next = PeekNext();
		if (next == EOLN_SYMBOL)
		{
			result.kind = T_ERROR;
			break;
		}
		if (next == DOUBLE_QUOTE)
		{
			std::string value = result.value;
			GetNextChar();
			if (!value.empty())
			{
				if (value.back() != BACKSLASH)
				{
					break;
				}
				else
				{
					result.value += next;
				}
			}
			else
			{
				break;
			}
		}
		else
		{
			result.value += GetNextChar();
		}
	}

	return result;
}

Token CLexer::ParseChar()
{
	Token result;

	result.kind = T_CHAR;

	while (HasNext() && T_ERROR != result.kind)
	{
		char ch = PeekNext();

		if (ch == EOLN_SYMBOL || ch == SINGLE_QUOTE)
		{
			break;
		}

		result.value += GetNextChar();

		if (result.value.length() == 1)
		{
			if (BACKSLASH == ch && PeekNext() == SINGLE_QUOTE)
			{
				char next = GetNextChar();
				if (PeekNext() != SINGLE_QUOTE)
				{
					result.kind = T_ERROR;
				}
				else
				{
					result.value += next;
				}
			}
			else if (!HasNext() || (BACKSLASH != ch && PeekNext() != SINGLE_QUOTE))
			{
				result.kind = T_ERROR;
			}
		}
		else if (result.value.length() == 2)
		{
			if (!HasNext() || PeekNext() != SINGLE_QUOTE)
			{
				result.kind = T_ERROR;
			}
		}
		else
		{
			result.kind = T_ERROR;
		}
	}

	size_t charLength = result.value.length();

	if (result.value.empty())
	{
		result.kind = T_ERROR;
		result.value = "Char should not be empty";
		if (HasNext())
		{
			GetNextChar();
		}

	}
	else if (T_ERROR == result.kind)
	{
		char next = PeekNext();
		while (HasNext() && !IsSeparator(next) && !isspace(next) && next != SINGLE_QUOTE && next != DOUBLE_QUOTE)
		{
			result.value += GetNextChar();
			next = PeekNext();
		}
		if (HasNext() && PeekNext() == SINGLE_QUOTE)
		{
			GetNextChar();
		}
	}
	else
	{
		if (HasNext())
		{
			GetNextChar();
		}
	}

	return result;
}

bool CLexer::IsSeparator(char ch) const
{
	return SEPARATORS.find(ch) != SEPARATORS.end();
}

bool CLexer::IsKeyword(const std::string& identifier) const
{
	return KEYWORDS.find(identifier) != KEYWORDS.end();
}

char CLexer::GetNextChar()
{
	if (m_position >= m_text.size())
	{
		throw std::out_of_range("End of string reached");
	}

	return m_text.at(m_position++);
}

char CLexer::PeekNext() const
{
	if (m_position >= m_text.size())
	{
		throw std::out_of_range("End of string reached");
	}

	return m_text.at(m_position);
}

size_t CLexer::GetInputSignal(char curr) const
{
	size_t result;
	if (isdigit(curr))
	{
		result = INPUT_DIGIT;
	}
	else if (curr == DOT)
	{
		result = INPUT_DOT;
	}
	else if (curr == 'e' || curr == 'E')
	{
		result = INPUT_E;
	}
	else if (curr == '+' || curr == '-')
	{
		result = INPUT_MARK;
	}
	else if (IsSeparator(curr) || isspace(curr))
	{
		result = INPUT_SEPARATOR;
	}
	else
	{
		result = INPUT_OTHER;
	}
	return result;
}
